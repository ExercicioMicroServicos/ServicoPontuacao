package com.itau.servicoPontuacao;


import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.itau.servicoPontuacao.repositories.PontuacaoRepository;


@SpringBootApplication
public class App 
{

	
    public static void main( String[] args )
    {
        SpringApplication.run(App.class, args);
    }
    
    
    
}
