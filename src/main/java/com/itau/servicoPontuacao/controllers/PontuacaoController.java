package com.itau.servicoPontuacao.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.servicoPontuacao.models.Pontuacao;
import com.itau.servicoPontuacao.repositories.PontuacaoRepository;

@RestController
@CrossOrigin
public class PontuacaoController {

	@Autowired
	PontuacaoRepository repository;
	
//	@RequestMapping(method=RequestMethod.GET, path="/ranking/{idJogo}")
//	public ResponseEntity<?> retornaRankingPorJogo(@PathVariable String idJogo){
//		
//		Optional<List<Pontuacao>> optionalPontuacoes = repository.findByIdJogo(idJogo);
//
//		if(!optionalPontuacoes.isPresent()) {
//			return ResponseEntity.notFound().build();
//		}
//		
//		return ResponseEntity.ok().body(optionalPontuacoes.get());		
//	}
	

	@RequestMapping(method=RequestMethod.POST, path="/pontuacao")
	public Pontuacao criarPontuacao(@Validated @RequestBody Pontuacao pontuacao) {
		return repository.save(pontuacao);
	}

	@RequestMapping(method=RequestMethod.PUT, path="/pontuacao")
	public Pontuacao adicionarPontuacao(@Validated @RequestBody Pontuacao pontuacao) {
		return repository.save(pontuacao);
	}
	
}
