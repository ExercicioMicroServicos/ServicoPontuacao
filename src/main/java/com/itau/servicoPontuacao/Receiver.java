package com.itau.servicoPontuacao;


import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

import com.itau.servicoPontuacao.models.Pontuacao;
import com.itau.servicoPontuacao.repositories.PontuacaoRepository;


public class Receiver implements MessageListener {
	
	PontuacaoRepository repository;

	public Receiver(PontuacaoRepository repository) {
		this.repository = repository;
	}
	
	public void onMessage(Message message) {
		MapMessage msg = (MapMessage) message;
		try {

			Pontuacao pont = new Pontuacao();
			
			pont.setNomeJogador(msg.getString("playerName").toString());
			pont.setIdJogo((Long)msg.getLong("gameId"));
			pont.setAcertos((int)msg.getInt("hits"));
			pont.setErros((int)msg.getInt("misses"));
			pont.setPontuacao((int)msg.getInt("total"));

			repository.save(pont);
			
			message.acknowledge();

		} catch (JMSException ex) {
			ex.printStackTrace();
		}
	}
}
