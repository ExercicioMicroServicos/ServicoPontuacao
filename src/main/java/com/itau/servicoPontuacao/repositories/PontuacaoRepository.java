package com.itau.servicoPontuacao.repositories;


import org.springframework.data.repository.CrudRepository;
import com.itau.servicoPontuacao.models.Pontuacao;


public interface PontuacaoRepository extends CrudRepository<Pontuacao, Long>{

}
